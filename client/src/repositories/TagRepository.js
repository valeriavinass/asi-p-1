import HTTP from "@/common/http";

const resource = "tags";

export default {
  async findAll() {
    const response = await HTTP.get(resource);
    return response.data;
  },
  async registerTag(name) {
    return (await HTTP.post("tags", name)).data;
  },
  async deleteTag(id) {
    return await HTTP.delete(`${resource}/${id}`);
  },
};
